/* 
day 1
return only one wrapper.
Always images in public folder
in img tag . means public folder

block element 
    it always starts from new line
    it takes all space

inline element 
    it takes space as it require    

br do not support style
in react we can store html element inside variable 
any thing that is written inside return are displayed in the browser
call variable inside tag using curly braces {}

javascript operation can be implemented inside tag using {}

*/

/* 
day2
components
it is a function
whose first letter must be capital
generally it return something (generally html tags)

FUNCTION VS COMPONENT
component is a function whose first letter is capital and return something
we call function using parenthesis but we call component as a html tag
custom component tag does not support styles

props 
passing value in component is called props
if props is other than string use curly braces{}



*/
/* 
DAY 3
if else cannot be use if,else,while,do-while inside tag but we can use array loop
variable cannot be define inside {}
{} return only things

ternary operator 
must have else part
boolean are not shown in the browser for logic have to be added
object cannot be used as react child <div></div>

array
=> wrapper are disappear
=> element are placed one by one without coma(,)
*/

/* 
DAY 4
In onCLick do not call function
*/

/* 
DAY 6
1)When normal Variable is changed page will not render
but when useState variable is changed 

2)if any state variable is updated then
the component will re execute
such that the state variable which is updated hold the updated value
and the other state variable holds the previous value

3)when the page gets render then all content of components gets removed the each element is placed one by one

useEffect
it is asynchronous function
it is always execute at last (after printing the content to the browser)
*/

/* 
DAY 7
At first render useEffect fun execute
but from second render useEffect fun depends on its dependency 
if one of the dependency changes useEffect fun will execute


cleanUp function 
clean up  function is a function return by useEffect 

1st render (component did mount)
it does not render in first render

2nd render (component did)
from 2nd render cleanUp fun execute

when useEffect fun gets executed at first cleanUp function will execute then the code above return wil execute

component did unmount (component remove)
when component is removed (during hide and show nothing gets execute but clean function will execute)

useEffect QUESTION ***
1) explain

*/

/* 
DAY 7

*/
/* 
DAY 8 
 <Route path="*">
          <div>404 error</div>
        </Route>
it will occur if route is other than define route
*/

/* 
DAY 9
other    value   e.target.value
checkbox  checked e.target.checked
radio     checked e.checked.value
*/
