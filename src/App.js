import "./App.css";
import Form1 from "./PracticeComponent/Form/Form1";
import FormikUserCreateFom from "./PracticeComponent/LearnFormik/FormikUserCreateFom";
import GetLocalStorage from "./PracticeComponent/LearnLocalHost/GetLocalStorage";
import LearnLocal from "./PracticeComponent/LearnLocalHost/LearnLocal";
import RemoveLocalStorage from "./PracticeComponent/LearnLocalHost/RemoveLocalStorage";
import GetSessionStorage from "./PracticeComponent/LearnSessionStorage/GetSessionStorage";
import LearnSessionStorage from "./PracticeComponent/LearnSessionStorage/LearnSessionStorage";
import RemoveSessionStorage from "./PracticeComponent/LearnSessionStorage/RemoveSessionStorage";
import UseRef1 from "./PracticeComponent/LearnUseRef/UseRef1";

// import Age from "./PracticeComponent/Age";
// import Detail from "./PracticeComponent/Detail";
// import Images from "./PracticeComponent/Images";
// import Info from "./PracticeComponent/Info";
// import Location from "./PracticeComponent/Location";
// import Name from "./PracticeComponent/Name";

function App() {
  // let [showCom, setShowCom] = useState(true);
  // console.log("first");
  return (
    <div>
      {/* <Name></Name>
      <Age></Age>
      <Location></Location> */}
      {/* <Detail name="aashutosh" age={28} address="bkt"></Detail> */}
      {/* <div className="success">success</div>
      <div className="failure">failure</div>
      <div className="info">info</div>
      <div className="warning">warning</div> */}
      {/* <Location
        country="Nepal"
        province="Bagmati"
        district="Bhaktapur"
        exactLocation="Sallaghari"
      ></Location> */}
      <br></br>
      {/* <Info
        name="aashutosh"
        age={22}
        fatherDetails={[{ fname: "jayapal", fage: 70 }]}
        favFood={["mutton", "chicken", "spinach"]}
      ></Info> */}
      <br></br>
      {/* <Images></Images> */}

      {/* <MapPractice></MapPractice> */}

      {/* <MapPractice2></MapPractice2> */}

      {/* <ButtonClick></ButtonClick> */}
      {/* <LearnUseState1></LearnUseState1> */}

      {/* <ImageShow></ImageShow> */}
      {/* <DisplayImage></DisplayImage> */}
      {/* <Toggle></Toggle> */}
      {/* <WhyUseState></WhyUseState> */}
      {/* <Increment></Increment> */}
      {/* <LearnUseEffect2></LearnUseEffect2> */}

      {/* {showCom ? <LearnCLeanUp></LearnCLeanUp> : null}
      <button
        onClick={() => {
          setShowCom(true);
        }}
      >
        show
      </button>
      <button
        onClick={() => {
          setShowCom(false);
        }}
      >
        hide
      </button> */}

      {/* <MyLinks></MyLinks> */}
      {/* <Nesting></Nesting> */}
      {/* <MyRoutes></MyRoutes> */}
      {/* <ReactRouter></ReactRouter> */}
      {/* <Form1></Form1> */}
      {/* <FormikForm></FormikForm> */}
      <FormikUserCreateFom></FormikUserCreateFom>

      {/* <UseRef1></UseRef1> */}
      {/* <LearnLocal></LearnLocal> */}
      {/* <GetLocalStorage></GetLocalStorage> */}
      {/* <RemoveLocalStorage></RemoveLocalStorage> */}
      {/* <LearnSessionStorage></LearnSessionStorage> */}
      {/* <GetSessionStorage></GetSessionStorage> */}

      {/* <RemoveSessionStorage></RemoveSessionStorage> */}
    </div>
  );
}

export default App;
