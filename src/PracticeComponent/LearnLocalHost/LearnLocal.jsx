import React from "react";

const LearnLocal = () => {
  let token = "123131";
  localStorage.setItem("token", token);
  localStorage.setItem("name", "aasu");
  localStorage.setItem("age", "23");
  localStorage.setItem("isMarried", "false");
  return <div>LearnLocal</div>;
};

export default LearnLocal;

/* 
local storage is the browser's memory for a particular url
the data in a local storage persist even when the session end (browser close) (tab close) 
*/
