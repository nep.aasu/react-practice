import React from "react";

const RemoveLocalStorage = () => {
  return (
    <div>
      <button
        onClick={() => {
          localStorage.removeItem("token");
        }}
      >
        Remove Token
      </button>
      <br />
      <button
        onClick={() => {
          localStorage.removeItem("name");
        }}
      >
        Remove name
      </button>
      <br />
      <button
        onClick={() => {
          localStorage.removeItem("age");
        }}
      >
        Remove age
      </button>
      <br />
      <button
        onClick={() => {
          localStorage.removeItem("isMarried");
        }}
      >
        Remove isMarried
      </button>
    </div>
  );
};

export default RemoveLocalStorage;
