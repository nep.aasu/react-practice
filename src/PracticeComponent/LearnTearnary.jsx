import React from "react";

const LearnTearnary = () => {
  let age;
  let marks;
  let calc = () => {
    if (age < 18) {
      return <div>underage</div>;
    } else if (age > 18 && age < 60) {
      return <div>adult</div>;
    } else {
      return <div>old</div>;
    }
  };
  return (
    <div>
      {age < 18 ? (
        <div></div>
      ) : age > 18 && age < 60 ? (
        <div>adult</div>
      ) : (
        <div></div>
      )}
      {marks >= 40 && marks < 60 ? (
        <div>third</div>
      ) : marks >= 60 && marks < 80 ? (
        <div>second</div>
      ) : (
        <div>first</div>
      )}
    </div>
  );
};

export default LearnTearnary;
