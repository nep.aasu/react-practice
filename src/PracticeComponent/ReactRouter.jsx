import React from "react";
import { Outlet, Route, Routes } from "react-router-dom";
import MyLinks from "./MyLinks";
import ProductForm from "./product/ProductForm";
import ReadAllProducts from "./product/ReadAllProducts";
import ReadSpecificProduct from "./product/ReadSpecificProduct";
import UpdateProduct from "./product/UpdateProduct";
import ReadAllStudents from "./student/ReadAllStudents";
import ReadSpecificStudents from "./student/ReadSpecificStudents";
import StudentCreate from "./student/StudentCreate";
import UpdateStudent from "./student/UpdateStudent";

const ReactRoutes = () => {
  return (
    <div>
      <Routes>
        <Route
          path="/"
          element={
            <div>
              <MyLinks></MyLinks>
              <Outlet></Outlet>
              {/* <div>this is footer</div> */}
            </div>
          }
        >
          <Route index element={<div> this is home page</div>}></Route>
          <Route
            path="products"
            element={
              <div>
                <Outlet></Outlet>
              </div>
            }
          >
            <Route index element={<ReadAllProducts></ReadAllProducts>}></Route>
            <Route
              path=":id"
              element={<ReadSpecificProduct></ReadSpecificProduct>}
            ></Route>
            <Route path="create" element={<ProductForm></ProductForm>}></Route>
            <Route
              path="update"
              element={
                <div>
                  <Outlet></Outlet>
                </div>
              }
            >
              <Route
                path=":id"
                element={<UpdateProduct></UpdateProduct>}
              ></Route>
            </Route>
          </Route>

          <Route
            path="students"
            element={
              <div>
                <Outlet></Outlet>
              </div>
            }
          >
            <Route index element={<ReadAllStudents></ReadAllStudents>}></Route>
            <Route
              path=":id"
              element={<ReadSpecificStudents></ReadSpecificStudents>}
            ></Route>
            <Route
              path="create"
              element={<StudentCreate></StudentCreate>}
            ></Route>
            <Route
              path="update"
              element={
                <div>
                  <Outlet></Outlet>
                </div>
              }
            >
              <Route
                path=":id"
                element={<UpdateStudent></UpdateStudent>}
              ></Route>
            </Route>
          </Route>
        </Route>

        <Route path="*" element={<div>404 page</div>}></Route>
      </Routes>
    </div>
  );
};

export default ReactRoutes;

// /   => this is home page
// /products => read all products
// /products/:id => detail page
// /products/create => create products
// /products/update/:id => products update
