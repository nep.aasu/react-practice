import React, { useEffect, useState } from "react";

const LearnCLeanUp = () => {
  let [count, setCount] = useState(0);
  useEffect(() => {
    console.log("use effect");
  }, [count]);
  return (
    <div>
      {count}
      <button
        onClick={
          (e) => {
          setCount(count + 1);
        }}
      >
        cleanup counter
      </button>
    </div>
  );
};

export default LearnCLeanUp;
