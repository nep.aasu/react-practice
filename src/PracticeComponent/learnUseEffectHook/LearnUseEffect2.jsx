import React, { useEffect, useState } from "react";

const LearnUseEffect2 = () => {
  let [count1, setCount1] = useState(0);
  let [count2, setCount2] = useState(0);
  //   useEffect(fun,[])
  useEffect(() => {
    console.log("i am use effect 1");
  }, [count1, count2]);
  useEffect(() => {
    console.log("i am use effect 2");
  }, []);
  useEffect(() => {
    console.log("first");
  });
  // for no dependency useEffect(fun) =>fun  will execute in each render
  // for empty dependency useEffect(fun,[]) fun will execute at first render only
  //    for [count1,count2 ] dependency useEffect(fun,[count1,count2]) but from second render useEffect fun depends on its dependency if one of the dependency changes useEffect fun will execute
  console.log("i am component");
  return (
    <div>
      count 1 {count1}
      <br />
      count 2 {count2}
      <br />
      <button
        onClick={(e) => {
          setCount1(count1 + 1);
        }}
      >
        counter
      </button>
      <br />
      <button
        onClick={(e) => {
          setCount2(count2 + 1);
        }}
      >
        counter
      </button>
    </div>
  );
};

export default LearnUseEffect2;
