import React, { useEffect, useState } from "react";

const LearnUseEffect1 = () => {
  let [count1, setCount1] = useState(0);
  let [count2, setCount2] = useState(0);
  //   useEffect(fun,[])
  useEffect(() => {
    console.log("i am use effect");
  }, [count1]);
  console.log("i am component");
  return (
    <div>
      {count1}
      {count2}
      <br />
      <button
        onClick={(e) => {
          setCount1(count1 + 1);
        }}
      >
        counter
      </button>
      <button
        onClick={(e) => {
          setCount2(count2 + 1);
        }}
      >
        counter
      </button>
    </div>
  );
};

export default LearnUseEffect1;
