import React from "react";
import { Route, Routes } from "react-router-dom";
import ProductForm from "./product/ProductForm";
import ReadAllProducts from "./product/ReadAllProducts";
import ReadSpecificProduct from "./product/ReadSpecificProduct";
import ReadAllStudents from "./student/ReadAllStudents";
import ReadSpecificStudents from "./student/ReadSpecificStudents";
import StudentCreate from "./student/StudentCreate";

const MyRoutes = () => {
  return (
    <div>
      <Routes>
        <Route path="/"></Route>
        <Route
          path="/products/create"
          element={<ProductForm></ProductForm>}
        ></Route>
        <Route
          path="/products"
          element={<ReadAllProducts></ReadAllProducts>}
        ></Route>
        <Route
          path="/products/:id"
          element={<ReadSpecificProduct></ReadSpecificProduct>}
        ></Route>
        <Route
          path="/students/create"
          element={<StudentCreate></StudentCreate>}
        ></Route>
        <Route
          path="/students"
          element={<ReadAllStudents></ReadAllStudents>}
        ></Route>
        <Route
          path="/students/:id"
          element={<ReadSpecificStudents></ReadSpecificStudents>}
        ></Route>
        <Route path="*" element={<div>404 error</div>}></Route>
      </Routes>
    </div>
  );
};

export default MyRoutes;
