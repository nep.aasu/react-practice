import React from "react";

const ButtonClick = () => {
  let buttonHandle = (e) => {
    console.log("button is clicked");
  };
  return (
    <div>
      {/* <button
        onClick={(e) => {
          console.log("button is clicked");
        }}
      >
        Click Me
      </button> */}
      <button onClick={buttonHandle}>Click me</button>
    </div>
  );
};

export default ButtonClick;
