import React from "react";

const DifferentDataEffect = () => {
  let name = "ram";
  let age = 40;
  let isMarried = true;
  let favFood = ["chicken", "spinach", "mutton"];
  let fatherInfo = { name: "shiva", age: 50 };
  return (
    <div>
      name is {name}
      name is {age}
      ismarried{isMarried ? "yes" : "no"}
      {favFood.map((value, i) => {
        return <div>{value}</div>;
      })}
    </div>
  );
};

export default DifferentDataEffect;
