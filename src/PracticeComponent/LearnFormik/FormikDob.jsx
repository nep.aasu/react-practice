import { Field } from "formik";
import React from "react";

const FormikDob = ({ name, label, onChange, required, ...props }) => {
  return (
    <div>
      <Field name={name}>
        {({ field, form, meta }) => {
          return (
            <div className="mb-4">
              <label htmlFor={name} className="text-sm text-gray-700">
                {label}
                {required ? (
                  <span className="text-red-600">*</span>
                ) : null}:{" "}
              </label>
              <input
                {...field}
                {...props}
                type="date"
                id={name}
                value={meta.value}
                onChange={!!onChange ? onChange : field.onChange}
                className=" rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
              ></input>
              {meta.touched && meta.error ? (
                <div className="text-red-600 text-sm">{meta.error}</div>
              ) : null}
            </div>
          );
        }}
      </Field>
    </div>
  );
};

export default FormikDob;
