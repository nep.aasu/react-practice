import { Field } from "formik";
import React from "react";

const FormikRadio = ({
  name,
  label,
  onChange,
  required,
  genders,
  ...props
}) => {
  return (
    <div>
      <Field name={name}>
        {({ field, form, meta }) => {
          return (
            <div className="mb-4">
              <label htmlFor={name} className="text-sm text-gray-700">
                {label}{" "}
                {required ? <span className="text-red-600">*</span> : null}:{" "}
              </label>
              {meta.touched && meta.error ? (
                <div className="text-red-600 text-sm">{meta.error}</div>
              ) : null}
              <div>
                {genders.map((item, i) => {
                  return (
                    <>
                      <label
                        htmlFor={item.value}
                        className="text-sm text-gray-700"
                      >
                        {item.label}
                      </label>
                      <input
                        {...field}
                        {...props}
                        type="radio"
                        id={item.value}
                        value={item.value}
                        checked={meta.value === item.value}
                        onChange={!!onChange ? onChange : field.onChange}
                      ></input>{" "}
                    </>
                  );
                })}
              </div>
            </div>
          );
        }}
      </Field>
    </div>
  );
};

export default FormikRadio;
