import { Form, Formik } from "formik";
import React from "react";
import * as yup from "yup";
import FormikCheckBox from "./FormikCheckBox";
import FormikInput from "./FormikInput";
import FormikRadio from "./FormikRadio";
import FormikSelect from "./FormikSelect";
import FormikTextArea from "./FormikTextArea";

const FormikForm = () => {
  let initialValues = {
    firstName: "",
    lastName: "",
    description: "",
    day: "",
    gender: "",
    isMarried: false,
  };
  let onSubmit = (value, other) => {
    console.log(value);
  };
  let validationSchema = yup.object({
    firstName: yup.string().required("firstName is required"),
    lastName: yup.string().required("lastName is required"),
    description: yup.string(),
    day: yup.string(),
    gender: yup.string().required("gender is required"),
    isMarried: yup.boolean().required("isMarried is required"),
  });
  const days = [
    {
      label: "Sunday",
      value: "day1",
    },
    {
      label: "Monday",
      value: "day2",
    },
    {
      label: "Tuesday",
      value: "day3",
    },
    {
      label: "Wednesday",
      value: "day4",
    },
    {
      label: "Thursday",
      value: "day5",
    },
    {
      label: "Friday",
      value: "day6",
    },
    {
      label: "Saturday",
      value: "day7",
    },
  ];
  const genders = [
    { label: "Male", value: "male" },
    { label: "Female", value: "female" },
    { label: "Other", value: "other" },
  ];
  return (
    <div>
      <Formik
        initialValues={initialValues}
        onSubmit={onSubmit}
        validationSchema={validationSchema}
      >
        {(formik) => {
          return (
            <Form>
              <FormikInput
                name="firstName"
                label="First Name"
                type="text"
                onChange={(e) => {
                  formik.setFieldValue("firstName", e.target.value);
                }}
                required={true}
                placeholder="Ram"
              ></FormikInput>
              <FormikInput
                name="lastName"
                label="Last Name"
                type="text"
                required={true}
                placeholder="Oli"
              ></FormikInput>
              <FormikTextArea
                name="description"
                label="Description"
                type="text"
              ></FormikTextArea>
              <FormikSelect
                name="day"
                label="Day"
                type="select"
                options={days}
              ></FormikSelect>

              <FormikRadio
                name="gender"
                label="Gender"
                genders={genders}
                required={true}
                className="mx-2"
              ></FormikRadio>
              <FormikCheckBox
                name="isMarried"
                label="Is Married"
                required={true}
                onChange={(e) => {
                  formik.setFieldValue("isMarried", e.target.checked);
                }}
              ></FormikCheckBox>
              <button
                type="submit"
                className="bg-green-600 py-2 px-3 rounded-md text-white hover:bg-green-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-green-600"
              >
                Submit
              </button>
            </Form>
          );
        }}
      </Formik>
    </div>
  );
};

export default FormikForm;
