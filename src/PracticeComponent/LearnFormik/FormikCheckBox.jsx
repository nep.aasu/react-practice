import { Field } from "formik";
import React from "react";

const FormikCheckBox = ({ name, label, onChange, required, ...props }) => {
  return (
    <div>
      <Field name={name}>
        {({ field, form, meta }) => {
          return (
            <div>
              <label htmlFor={name}>{label}: </label>
              {meta.touched && meta.error ? (
                <div className="text-red-600 text-sm">{meta.error}</div>
              ) : null}
              <input
                type="checkbox"
                id={name}
                checked={meta.value}
                onChange={!!onChange ? onChange : field.onChange}
              ></input>
            </div>
          );
        }}
      </Field>
    </div>
  );
};

export default FormikCheckBox;
