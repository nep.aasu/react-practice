import { Form, Formik } from "formik";
import React from "react";
import * as yup from "yup";
import FormikDob from "./FormikDob";
import FormikInput from "./FormikInput";
import FormikRadio from "./FormikRadio";
import FormikSelect from "./FormikSelect";

const FormikUserCreateFom = () => {
  let initialValues = {
    fullName: "",
    email: "",
    password: "",
    role: "",
    gender: "",
    dob: "",
  };
  let onSubmit = (value, other) => {
    console.log(value);
  };
  const fullNameRegex = /^[a-zA-Z]+ [a-zA-Z]+$/;
  const emailRegex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;
  const passwordRegex =
    /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/;
  let validationSchema = yup.object({
    fullName: yup
      .string()
      .required("Full Name is required")
      .min(3, "Must be at least 3 character")
      .matches(fullNameRegex, "Invalid full name format"),

    email: yup.string().required("Email is required").matches(emailRegex),

    password: yup
      .string()
      .required("Password is required")
      .min(8, "Must be at least 8 character")
      .matches(passwordRegex, " 1 uppercase & 1 lowercase character  1 number"),

    gender: yup.string().required("Gender is required"),
    dob: yup
      .date()
      .required("Date of Birth is required")
      .test(
        "is-adult",
        "You must be 18 years or older",
        (value) => value && new Date().getFullYear() - value.getFullYear() >= 18
      ),
    role: yup.string().required("Role is required"),
  });
  const roles = [
    {
      label: "Super Admin",
      value: "superAdmin",
    },
    {
      label: "Admin",
      value: "admin",
    },
    {
      label: "User",
      value: "user",
    },
  ];
  const genders = [
    {
      label: "Male",
      value: "male",
    },
    {
      label: "Female",
      value: "female",
    },
    {
      label: "Other",
      value: "other",
    },
  ];
  return (
    <div>
      <Formik
        initialValues={initialValues}
        onSubmit={onSubmit}
        validationSchema={validationSchema}
      >
        {(formik) => {
          return (
            <Form>
              <FormikInput
                name="fullName"
                label="Full Name"
                type="text"
                onChange={(e) => {
                  formik.setFieldValue("fullName", e.target.value);
                }}
                required={true}
                placeholder="Ram"
              ></FormikInput>
              <FormikInput
                name="email"
                label="Email"
                type="email"
                required={true}
                placeholder="ram@gmail.com"
              ></FormikInput>
              <FormikInput
                name="password"
                label="Password"
                type="password"
                required={true}
              ></FormikInput>
              <FormikSelect
                name="role"
                label="Role"
                type="select"
                options={roles}
              ></FormikSelect>

              <FormikRadio
                name="gender"
                label="Gender"
                genders={genders}
                required={true}
                className="mx-2"
              ></FormikRadio>
              <FormikDob
                name="dob"
                label="Date of Birth"
                required={true}
              ></FormikDob>
              <button
                type="submit"
                className="bg-green-600 py-2 px-3 rounded-md text-white hover:bg-green-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-green-600"
              >
                Submit
              </button>
            </Form>
          );
        }}
      </Formik>
    </div>
  );
};

export default FormikUserCreateFom;
