import { Field } from "formik";
import React from "react";

const FormikSelect = ({
  name,
  label,
  type,
  onChange,
  required,
  options,
  ...props
}) => {
  return (
    <div>
      <Field name={name}>
        {({ field, form, meta }) => {
          return (
            <div className="mb-4">
              <label htmlFor={name} className="text-sm text-gray-700">
                {label}{" "}
                {required ? <span className="text-red-600">*</span> : null}:{" "}
              </label>
              <select
                {...field}
                {...props}
                name="role"
                id="role"
                value={meta.value}
                onChange={!!onChange ? onChange : field.onChange}
                className=" rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
              >
                {options.map((item, i) => {
                  return <option value={item.value}>{item.label}</option>;
                })}
              </select>
            </div>
          );
        }}
      </Field>
    </div>
  );
};

export default FormikSelect;
