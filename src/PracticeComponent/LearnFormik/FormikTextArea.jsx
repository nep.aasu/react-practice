import { Field } from "formik";
import React from "react";

const FormikTextArea = ({
  name,
  label,
  type,
  onChange,
  required,
  ...props
}) => {
  return (
    <div>
      <Field name={name}>
        {({ field, form, meta }) => {
          return (
            <div>
              <label htmlFor={name} className="text-sm text-gray-700">
                {label}
                {required ? (
                  <span className="text-red-600">*</span>
                ) : null}:{" "}
              </label>
              <textarea
                {...field}
                {...props}
                type={type}
                placeholder={props.placeholder}
                id={name}
                value={meta.value}
                onChange={!!onChange ? onChange : field.onChange}
              ></textarea>
              {meta.touched && meta.error ? (
                <div className="text-red-600 text-sm">{meta.error}</div>
              ) : null}
            </div>
          );
        }}
      </Field>
    </div>
  );
};

export default FormikTextArea;
