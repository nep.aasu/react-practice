import React from "react";

const RemoveSessionStorage = () => {
  return (
    <div>
      <button
        onClick={() => {
          sessionStorage.removeItem("token");
        }}
      >
        remove token
      </button>
    </div>
  );
};

export default RemoveSessionStorage;
