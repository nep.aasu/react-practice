import React from "react";

const MapPractice = () => {
  let bestFriends = ["raam", "harey", "shyamey"];
  let bestfriend = () => {
    let array = bestFriends.map((item, i) => {
      return <div>my best friend is {item}</div>;
    });
    return array;
  };
  return (
    <div>
      {bestFriends.map((value, i) => {
        return <div>my best friend is {value}</div>;
      })}
      {bestfriend()}
    </div>
  );
};

export default MapPractice;
