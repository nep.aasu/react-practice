import React, { useState } from "react";

const Increment = () => {
  let [count, setCount] = useState(0);
  return (
    <div>
      {count}
      <br />
      <button
        onClick={() => {
          setCount(count + 1);
        }}
      >
        counter
      </button>
    </div>
  );
};

export default Increment;
