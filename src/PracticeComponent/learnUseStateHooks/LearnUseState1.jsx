import React, { useState } from "react";

const LearnUseState1 = () => {
  let [name, setName] = useState("ram");
  let [age, setAge] = useState(30);
  let handleButton = (e) => {
    setName("hari");
  };
  let handleAge = (e) => {
    setAge(33);
  };
  let [num, newNum] = useState(0);
  let incrementButton = (e) => {
    newNum(num < 10 ? num + 1 : 10);
  };
  let decrementButton = (e) => {
    newNum(num < 0 ? num - 1 : 0);
  };
  let resetButton = (e) => {
    newNum(0);
  };

  return (
    <div>
      my name is {name}
      <br></br>
      <button onClick={handleButton}>change name</button>
      <p> age : {age}</p>
      <br />
      <button onClick={handleAge}>change button</button>
      <br />
      <p>number count {num}</p>
      <button onClick={incrementButton}>increment</button>
      <button onClick={decrementButton}>decrement</button>
      <button onClick={resetButton}>Reset</button>
      <br />
    </div>
  );
};

export default LearnUseState1;
