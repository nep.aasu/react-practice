import React, { useState } from "react";
import Image from "./Image";

const ImageShow = () => {
  let [image, newImage] = useState(<Image></Image>);
  let [showImage, newShowImage] = useState(true);
  let showButton = (e) => {
    newImage(<Image></Image>);
  };
  let hideButton = (e) => {
    newImage(null);
  };
  let showButton1 = (e) => {
    newShowImage(true);
  };
  let hideButton1 = (e) => {
    newShowImage(false);
  };

  return (
    <div>
      {/* {image} */}
      {showImage ? <img src="./logo192.png" alt="img"></img> : null}
      {/* <button onClick={showButton}>Show Img</button> */}
      {/* <button onClick={hideButton}>Hide Img</button> */}
      <br />
      <button onClick={(showButton1, hideButton1)}>Show Img1</button>
      <button onClick={hideButton1}>Hide Img1</button>
    </div>
  );
};

export default ImageShow;
