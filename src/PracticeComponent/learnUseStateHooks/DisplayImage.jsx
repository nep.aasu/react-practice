import React, { useState } from "react";

const DisplayImage = () => {
  let [showImage, setShowImage] = useState(true);
  let handleImage = (isDisplay) => {
    return (e) => {
      setShowImage(isDisplay);
    };
  };
  return (
    <div>
      {showImage ? <img src="./logo192.png"></img> : null}
      <br />
      <button onClick={handleImage(true)}>show</button>
      <button onClick={handleImage(false)}>hide</button>
    </div>
  );
};

export default DisplayImage;
