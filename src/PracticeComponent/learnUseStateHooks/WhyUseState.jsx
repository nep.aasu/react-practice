import React, { useState } from "react";

const WhyUseState = () => {
  let [name, setName] = useState("ram");

  return (
    <div>
      {name}
      <br />
      <button
        onClick={(e) => {
          setName("hair");
        }}
      >
        click me
      </button>
    </div>
  );
};

export default WhyUseState;
