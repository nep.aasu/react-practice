import React from "react";
import { Outlet, Route, Routes } from "react-router-dom";

const Nesting = () => {
  return (
    <div>
      <Routes>
        <Route
          path="/"
          element={
            <div>
              <Outlet></Outlet>
            </div>
          }
        >
          <Route index element={<div>Home Page</div>}></Route>
          <Route path="create" element={<div>Create Blog</div>}></Route>
          <Route path=":id" element={<div>detail Blog</div>}></Route>
          <Route
            path="update"
            element={
              <div>
                <Outlet></Outlet>
                update
              </div>
            }
          >
            <Route path=":id" element={<div>Update blog</div>}></Route>
          </Route>
        </Route>
      </Routes>
    </div>
  );
};

export default Nesting;
