import React, { useRef } from "react";

const UseRef1 = () => {
  let ref1 = useRef();
  let ref2 = useRef();
  let ref3 = useRef();

  return (
    <div>
      <input ref={ref1} />
      <input ref={ref2} />
      <input ref={ref3} />
      <button
        onClick={() => {
          ref1.current.focus();
        }}
      >
        focus 1
      </button>
      <button
        onClick={() => {
          ref1.current.blur();
        }}
      >
        blur 2
      </button>
      <button
        onClick={() => {
          ref1.current.style.backgroundColor = "yellow";
        }}
      >
        color 3
      </button>
    </div>
  );
};

export default UseRef1;
