import axios from "axios";
import React, { useEffect, useState } from "react";
import { useNavigate, useParams, useSearchParams } from "react-router-dom";

const ReadSpecificProduct = () => {
  // let params = useParams();
  // console.log(params);
  // let id = params.id;

  // let [query] = useSearchParams(id);
  // console.log(query.get("name"));
  // console.log(query.get("age"));
  // let navigate = useNavigate();
  let [products, setProducts] = useState({});
  let params = useParams();

  let getProducts = async () => {
    let result = await axios({
      url: `http://localhost:8000/products/${params.id}`,
      method: "GET",
    });
    setProducts(result.data.result);
  };
  useEffect(() => {
    getProducts();
  }, []);
  return (
    <div>
      <p>Product is {products.name}</p>
      <p>Product price is {products.price}</p>
      <p>Product quantity is {products.quantity}</p>
    </div>
  );
};

export default ReadSpecificProduct;
