import axios from "axios";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
const MySwal = withReactContent(Swal);
const ReadAllProducts = () => {
  let [products, setProducts] = useState([]);
  let navigate = useNavigate();

  let getAllProducts = async () => {
    try {
      let result = await axios({
        url: `http://localhost:8000/products`,
        method: "get",
      });
      setProducts(result.data.result);
    } catch (error) {
      console.log(error.response.data.message);
    }
  };

  useEffect(() => {
    getAllProducts();
  }, []);

  let handleDelete = (id) => {
    return (e) => {
      MySwal.fire({
        title: "Confirmation",
        text: "Are you sure you want Delete?",
        showCancelButton: true,
        confirmButtonText: "Delete",
        cancelButtonText: "Cancel",
      }).then(async (result) => {
        if (result.isConfirmed) {
          try {
            let result = await axios({
              url: `http://localhost:8000/products/${id}`,
              method: "DELETE",
            });
            getAllProducts();
            toast.success(result.data.message);
          } catch (error) {
            toast.error(error.response.data.message);
          }
        } else if (result.isDismissed) {
          console.log("cancel button is clicked");
        }
      });
    };
  };
  let handleView = (id) => {
    return (e) => {
      navigate(`/products/${id}`);
    };
  };
  let handleUpdate = (id) => {
    return (e) => {
      navigate(`/products/update/${id}`);
    };
  };
  return (
    <>
      <div className="mx-auto max-w-2xl px-4 py-16 sm:px-6 sm:py-24 lg:max-w-7xl lg:px-8">
        <ToastContainer />

        <div className="grid grid-cols-1 gap-x-6 gap-y-10 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 xl:gap-x-8">
          {products.map((item, i) => {
            return (
              <div key={i} className="p-2 border-solid border-2 border-sky-500">
                <p>product is {item.name}</p>
                <p>product price is {item.price}</p>
                <p>product quantity is {item.quantity}</p>
                <button
                  onClick={handleUpdate(item._id)}
                  className=" mr-2 bg-green-500 px-2 py-1 text-sm rounded"
                >
                  edit
                </button>
                <button
                  className="mr-2 bg-blue-500 px-2 py-1 text-sm rounded "
                  onClick={handleView(item._id)}
                >
                  view
                </button>
                <button
                  className=" mr-2 bg-red-500 px-2 py-1 text-sm  rounded"
                  onClick={handleDelete(item._id)}
                >
                  delete
                </button>
              </div>
            );
          })}
        </div>
      </div>
    </>
  );
};

export default ReadAllProducts;
