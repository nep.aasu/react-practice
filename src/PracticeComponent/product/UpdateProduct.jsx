import axios from "axios";
import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const UpdateProduct = () => {
  let [name, setName] = useState();
  let [price, setPrice] = useState("");
  let [quantity, setQuantity] = useState("");

  let params = useParams();
  let navigate = useNavigate();

  let onSubmit = async (e) => {
    e.preventDefault();
    let data = {
      name: name,
      price: price,
      quantity: quantity,
    };

    // console.log(data);
    try {
      let result = await axios({
        url: `http://localhost:8000/products/${params.id}`,
        method: "PATCH",
        data: data,
      });

      navigate(`/products/${params.id}`);
    } catch (error) {
      toast.error(error.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    }
  };
  let getProduct = async () => {
    try {
      let result = await axios({
        url: `http://localhost:8000/products`,
        method: "get",
      });
      let data = result.data.result;
      setName(data.name);
    } catch (error) {
      console.log(error.response.data.message);
    }
  };

  useEffect(() => {
    getProduct();
  }, []);

  return (
    <div>
      <ToastContainer />
      <form onSubmit={onSubmit}>
        <div>
          <label htmlFor="name" className="text-sm text-gray-700">
            Product Name
          </label>
          <input
            type="text"
            placeholder="Eg: 
           chitu"
            id="name"
            value={name}
            className="block rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
            onChange={(e) => {
              setName(e.target.value);
            }}
          ></input>
        </div>

        <div>
          <label htmlFor="price" className="text-sm text-gray-700">
            Price:
          </label>
          <input
            type="number"
            id="price"
            value={price}
            onChange={(e) => {
              setPrice(e.target.value);
            }}
            className="block rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
          ></input>
        </div>

        <div>
          <label htmlFor="quantity" className="text-sm text-gray-700">
            Quantity:
          </label>
          <input
            type="number"
            id="quantity"
            value={quantity}
            className="block rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
            onChange={(e) => {
              setQuantity(e.target.value);
            }}
          ></input>
        </div>
        <br />

        <button
          type="submit"
          className="rounded-md bg-green-600 px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-green-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
        >
          Update
        </button>
      </form>
    </div>
  );
};

export default UpdateProduct;
