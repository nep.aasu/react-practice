import React from "react";
import { products } from "../productData";

const MapPractice2 = () => {
  let task1 = () => {
    let desireOutput = products.map((item, i) => {
      return <li>{item.title}</li>;
    });
    return desireOutput;
  };

  let task2 = () => {
    let desireOutput = products
      .filter((value, i) => {
        if (value.price > 2000) {
          return true;
        }
      })
      .map((item, i) => {
        return (
          <li>
            {item.title} prices NRS. {item.price} and its category is{" "}
            {item.category}
          </li>
        );
      });
    return desireOutput;
  };

  let task3 = () => {
    let desireOutput = products
      .filter((value, i) => {
        if (value.category === "Books") {
          return true;
        }
      })
      .map((item, i) => {
        return (
          <li>
            {item.title} costs NRS. {item.price} and its category is{" "}
            {item.category}
          </li>
        );
      });
    return desireOutput;
  };

  let task4 = () => {
    let desireOutput = products
      .map((value, i) => {
        return value.price;
      })
      .reduce((pre, cur) => {
        return pre + cur;
      }, 0);
    return desireOutput;
  };

  let task5 = () => {
    let desireOutput = products.map((value, i) => {
      return (
        <p>
          {value.title} costs NRs. {value.price}
        </p>
      );
    });
    return desireOutput;
  };
  return (
    <div>
      <h1>The products in the store are</h1>
      {/* <p>{task1()}</p> */}
      {/* <p>{task2()}</p> */}
      {/* <p>{task3()}</p> */}
      <p>The total price of all product is NRS.{task4()}</p>
      <p>{task5()}</p>
    </div>
  );
};

export default MapPractice2;
