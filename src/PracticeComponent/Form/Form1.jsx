import React, { useState } from "react";

const Form1 = () => {
  let onSubmit = (e) => {
    e.preventDefault();
    let data = {
      firstName: firstName,
      lastName: lastName,
      email: email,
      password: password,
      dob: dob,
      isMarried: isMarried,
      day: day,
      gender: gender,
    };
    console.log(data);
  };
  let [firstName, setFirstName] = useState("");
  let [lastName, setLastName] = useState("");
  let [email, setEmail] = useState("");
  let [password, setPassword] = useState("");
  let [dob, setDob] = useState("");
  let [isMarried, setIsMarried] = useState(false);
  let [day, setDay] = useState("day1");
  let [gender, setGender] = useState("male");

  let days = [
    {
      label: "Sunday",
      value: "day1",
    },
    {
      label: "Monday",
      value: "day2",
    },
    {
      label: "Tuesday",
      value: "day3",
    },
    {
      label: "Wednesday",
      value: "day4",
    },
    {
      label: "Thursday",
      value: "day5",
    },
    {
      label: "Friday",
      value: "day6",
    },
    {
      label: "Saturday",
      value: "day7",
    },
  ];

  let genders = [
    { label: "Male", value: "male" },
    { label: "Female", value: "female" },
    { label: "Other", value: "other" },
  ];
  return (
    <div>
      <form onSubmit={onSubmit}>
        <div>
          <label htmlFor="firstName">First Name: </label>
          <input
            type="text"
            placeholder="Eg: 
           chitu"
            id="firstName"
            value={firstName}
            onChange={(e) => {
              setFirstName(e.target.value);
            }}
          ></input>
        </div>
        <br />
        <div>
          <label htmlFor="lastName">Last Name: </label>
          <input
            type="text"
            placeholder="Eg: 
           swami"
            id="lastName"
            value={lastName}
            onChange={(e) => {
              setLastName(e.target.value);
            }}
          ></input>
        </div>
        <br />
        <div>
          <label htmlFor="email">Email: </label>
          <input
            type="email"
            placeholder="Eg: 
           abc@gmail.com"
            id="email"
            value={email}
            onChange={(e) => {
              setEmail(e.target.value);
            }}
          ></input>
        </div>
        <br />
        <div>
          <label htmlFor="name">Password: </label>
          <input
            type="password"
            value={password}
            onChange={(e) => {
              setPassword(e.target.value);
            }}
          ></input>
        </div>
        <br />
        <div>
          <label htmlFor="dob">DOB: </label>
          <input
            type="date"
            value={dob}
            id="dob"
            onChange={(e) => {
              setDob(e.target.value);
            }}
          ></input>
        </div>

        <div>
          <br />
          <label htmlFor="isMarried">isMarried: </label>
          <input
            type="checkbox"
            id="isMarried"
            checked={isMarried === true}
            onChange={(e) => {
              setIsMarried(e.target.checked);
            }}
          ></input>
        </div>
        <br />
        <div>
          <label htmlFor="day">Day: </label>
          <select
            name="day"
            id="day"
            value={day}
            onChange={(e) => {
              setDay(e.target.value);
            }}
          >
            {days.map((item, i) => {
              return <option value={item.value}>{item.label}</option>;
            })}
          </select>
        </div>
        <div>
          <label htmlFor="male">gender</label>
          <div>
            {/* {label:"Male", value:"male"}, */}

            {genders.map((item, i) => {
              return (
                <>
                  <label htmlFor={item.value}>{item.label}</label>
                  <input
                    type="radio"
                    id={item.value}
                    value={item.value}
                    checked={gender === item.value}
                    onChange={(e) => {
                      setGender(e.target.value);
                    }}
                  ></input>
                </>
              );
            })}
          </div>
        </div>

        <button
          type="submit"
          className="rounded-md bg-green-600 px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-green-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-green-600"
        >
          send
        </button>
      </form>
    </div>
  );
};

export default Form1;
