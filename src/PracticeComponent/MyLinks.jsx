import React from "react";
import { NavLink } from "react-router-dom";

const MyLinks = () => {
  return (
    <div>
      <NavLink
        to="http://localhost:3000/products/create"
        style={{ marginRight: "20px" }}
      >
        Products create
      </NavLink>
      <NavLink
        to="http://localhost:3000/products"
        style={{ marginRight: "20px" }}
      >
        Products
      </NavLink>
      <NavLink
        to="http://localhost:3000/students"
        style={{ marginRight: "20px" }}
      >
        students
      </NavLink>
      <NavLink
        to="http://localhost:3000/students/create"
        style={{ marginRight: "20px" }}
      >
        students create
      </NavLink>

      {/* <MyRoutes></MyRoutes> */}
      {/* <ReactRouter></ReactRouter> */}
    </div>
  );
};

export default MyLinks;
