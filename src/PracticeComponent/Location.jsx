import React from "react";

const Location = (props) => {
  return (
    <>
      <div>Country: {props.country}</div>
      <div>Province: {props.province}</div>
      <div>District: {props.district}</div>
      <div>exactLocation: {props.exactLocation}</div>
    </>
  );
};

export default Location;
