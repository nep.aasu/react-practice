import React from "react";
import ReactRouter from "./PracticeComponent/ReactRouter";

const MyApp = () => {
  return (
    <div>
      <ReactRouter></ReactRouter>
    </div>
  );
};

export default MyApp;
